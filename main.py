import os
from PIL import Image, ImageOps
import requests
from io import BytesIO

import random
import qrcode


display_width = 1872
display_height = 1404

def prep_for_display(im):
    # Resize
    print(im.format, im.size, im.mode)
    im = ImageOps.pad(im, (display_width,display_height),Image.BICUBIC,(255,255,255,255))
    print(im.format, im.size, im.mode)

    # Grayscale
    im = ImageOps.grayscale(im)
    return im

def add_signature(background_image):
    path_to_file = "signature.png"
    foreground = Image.open(path_to_file)

    background_image.paste(foreground, (display_width-foreground.size[0]-20, display_height-foreground.size[1]-20), foreground)
    
    return background_image

def load_from_url(url):
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))
    return img

def load_from_file(file_name):
    identificator = file_name.split(".")[0]
    path_to_file = os.path.join("img", file_name) # Combine folder + File
    im = Image.open(path_to_file)
    return im, identificator

def pick_random_image():
    list = os.listdir("img")
    return random.choice(list)

def add_qr(im, identificator):

    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=3,
        border=1,
    )
    full_url = 'https://www.perfectstrokeproject.com/strokes/details/'+identificator
    qr.add_data(full_url)
    qr.make(fit=True)

    qrimg = qr.make_image(fill_color="grey", back_color="white")

    im.paste(qrimg, (20, display_height-qrimg.size[1]-20))
    return im
    
    

random = pick_random_image()
im, identificator = load_from_file(random)
print (identificator)
# im = load_from_url("https://source.unsplash.com/random")
im = prep_for_display(im)
im = add_signature(im)
im = add_qr(im, identificator)

im.show()
quit()


# crontab
#########

# Open crontab:
# crontab -e

# 0 * * ? * * -> everyminute
# 0 0 * ? * * -> Every hour
# 0 0 6 * * ? -> every day at 6am


# Lauch at boot
#################

# https://www.instructables.com/id/Raspberry-Pi-Launch-Python-script-on-startup/